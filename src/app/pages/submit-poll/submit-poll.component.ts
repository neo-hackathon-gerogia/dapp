import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {PopDialogComponent} from '../../shared/pop-dialog/pop-dialog.component';
import {NeoService} from '../../services/neo.service';

@Component({
  selector: 'app-submit-poll',
  templateUrl: './submit-poll.component.html',
  styleUrls: ['./submit-poll.component.css']
})

export class SubmitPollComponent implements OnInit {

  idN: any;
  name: any;
  surName: any;
  answer: any;


  idToPollMap = {
    1: {
      name: 'Should parliament be moved back to Tbilisi',
      headerTitle: 'Goverment poll',
      bottomTitle: 'Ends 25, october',
      answers: ['yes', 'no']
    }
    // ,
    // 2: {
    //   name: ' Should goverment stop financing Georgian Football clubs',
    //   headerTitle: 'Goverment poll',
    //   bottomTitle: 'ends 22,november',
    //   answers: ['yes', 'no']
    // }
  };

  constructor(private route: ActivatedRoute, public dialog: MatDialog, private router: Router, private neoService: NeoService) {
  }

  id: any;

  scriptHash: any;
  operation: any;

  openDialog(): void {
    this.neoService.getStaticData().subscribe(staticData => {
      let stResult: any;
      stResult = staticData;
      this.scriptHash = stResult.scriptHash;
      this.operation = stResult.operation;
      this.operation = 'vote';
      this.neoService.getAddress().subscribe(address => {
        const alertQuestion = 'Do you want to vote with address ' + address + ' ?';
        this.neoService.invoke(this.scriptHash, this.operation, alertQuestion, [this.answer, address]).subscribe(data => {
          const dialogRef = this.dialog.open(PopDialogComponent, {
            width: '250px',
            data: {name: ''}
          });
          dialogRef.afterClosed().subscribe(result => {
            this.router.navigate(['/home']);
          });
        });
      });
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
        this.id = params['id'];
      }
    );
  }

}
