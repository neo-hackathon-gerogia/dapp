import {Injectable} from '@angular/core';
import {from, Observable} from 'rxjs';
import {of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/internal/operators';


@Injectable({
  providedIn: 'root'
})
export class NeoService {

  nos;
  address;

  constructor(private http: HttpClient) {
    if ((window as any).NOS) {
      this.nos = (window as any).NOS.V1;
    }
    this.getAddress().subscribe(
      address =>
        this.address = address
    );
  }

  isConnected() {
    return this.nos;
  }

  getStaticData() {
    return this.http.get('https://private-191c44-nos.apiary-mock.com/questions').pipe(tap(data => {
      return data;
    }));
  }

  getAddress(): Observable<any> {
    if (this.nos) {
      return from(this.nos.getAddress());
    }
  }


  invoke(scriptHash: string, operation: string, alertQuestion: string, args?: any[]): Observable<any> {
    if (this.nos) {
      return from(this.nos.invoke({scriptHash, operation, args}));
    }
  }

}
