import { Routes } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { SubmitPollComponent } from './pages/submit-poll/submit-poll.component';

export const appRoutes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'}, 

  {
    path: 'home',
    component: HomePageComponent
  },

  {
    path: 'submit-poll/:id',
    component: SubmitPollComponent
  },

];
